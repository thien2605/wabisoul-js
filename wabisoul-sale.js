let version = '1.0.4';

const checkVersionLocal = () => {
  const currentVersion = localStorage.getItem('version');
  if (currentVersion !== version) {
    localStorage.clear();
    localStorage.setItem('cart', '[]');

    localStorage.setItem('version', version);
  }
};
checkVersionLocal();
// Custom Jquery functon start
(function ($) {
  $.fn.inputFilter = function (inputFilter) {
    return this.on(
      'input keydown keyup mousedown mouseup select contextmenu drop',
      function () {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty('oldValue')) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = '';
        }
      }
    );
  };
})(jQuery);
const sendSubmissionUrl =
  'https://api.hsforms.com/submissions/v3/integration/submit/21323330/2ed2c9a2-3d15-4210-a39f-3cf578adc084';
// Custom Jquery functon end
// Utils start
const numberWithCommas = (x) =>
  x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
const isPhone = (value) =>
  /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(value);
const isEmail = (value) =>
  /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
const isEmpty = (value) => value === '' || !value;

const onHomePage = window.location.pathname === '/';
const isEmptyArray = (arr) => !arr || arr.length === 0;
const lockBodyScroll = () => {
  document.querySelectorAll('.scrolltarget').forEach((t) => {
    bodyScrollLock.disableBodyScroll(t);
  });
};
const unlockBodyScroll = () => {
  document.querySelectorAll('.scrolltarget').forEach((t) => {
    bodyScrollLock.enableBodyScroll(t);
  });
};

// Utils end

// Models start
const colors = {
  oak: 'oak',
  walnut: 'walnut',
  black: 'black',
};
const productInfos = [
  {
    id: 'single-stand-riser',
    name: 'Single Stand Riser',
    price: 890000,
    sort: 4,
    beforePrice: {
      [colors.oak]: 890000,
      [colors.walnut]: 990000,
    },
    finalPrice: {
      [colors.oak]: 890000,
      [colors.walnut]: 990000,
    },
    defaultColor: colors.oak,
    img: {
      [colors.oak]:
        'https://uploads-ssl.webflow.com/61cc0644ab837a35f9e293e7/61ea4c8011cba26167e24e82_6.jpg',
      [colors.walnut]:
        'https://uploads-ssl.webflow.com/61cc0644ab837a35f9e293e7/61ea4c96b77c7e2040c6905c_2.jpg',
    },
  },
  {
    id: 'dual-desk-shelf',
    name: 'Dual Desk Shelf',
    price: 1190000,
    sort: 3,
    defaultColor: colors.oak,
    beforePrice: {
      [colors.oak]: 1190000,
      [colors.walnut]: 1290000,
    },
    finalPrice: {
      [colors.oak]: 1190000,
      [colors.walnut]: 1290000,
    },
    img: {
      [colors.oak]:
        'https://uploads-ssl.webflow.com/61cc0644ab837a35f9e293e7/61ea4d26c88e9219d82f6053_14.jpg',
      [colors.walnut]:
        'https://uploads-ssl.webflow.com/61cc0644ab837a35f9e293e7/61ea4d407060b91c6a099cef_10.jpg',
    },
  },
  {
    id: 'onyx-tray',
    name: 'Onyx Tray',
    price: 390000,
    sort: 2,
    beforePrice: {
      [colors.black]: 390000,
    },
    finalPrice: {
      [colors.black]: 390000,
    },
    defaultColor: colors.black,
    img: {
      [colors.black]:
        'https://uploads-ssl.webflow.com/61cc0644ab837a35f9e293e7/61ea4ce3701a3f308b5266c2_18.jpg',
    },
  },
  {
    id: 'onyx-tray-free',
    name: 'Onyx Tray Free',
    price: 0,
    removePrice: 390000,
    sort: 5,
    beforePrice: {
      [colors.black]: 0,
    },
    finalPrice: {
      [colors.black]: 0,
    },
    defaultColor: colors.black,
    img: {
      [colors.black]:
        'https://uploads-ssl.webflow.com/61cc0644ab837a35f9e293e7/61ea4ce3701a3f308b5266c2_18.jpg',
    },
  },
];
const getCurrentProduct = () => {
  if (onHomePage) {
    return undefined;
  }
  const id = window.location.pathname.split('/')[1];
  return productInfos.find(({ id: productId }) => productId === id);
};
const currentProduct = getCurrentProduct();
const handleInitData = () => {
  if (onHomePage) return;
  $('.modal-product-quantity').text(1);
  $('.modal-product-color').text(currentProduct.defaultColor);
  $('.modal-product-name').text(currentProduct.name);
  $('.vnd-wrapper .h4').text(
    numberWithCommas(currentProduct.finalPrice[currentProduct.defaultColor])
  );
  $('.text-price-before-heading').text(
    numberWithCommas(currentProduct.beforePrice[currentProduct.defaultColor])
  );
  $('.text-price-heading').text(
    numberWithCommas(currentProduct.finalPrice[currentProduct.defaultColor])
  );
  $('.text-detail-total').text(
    numberWithCommas(currentProduct.finalPrice[currentProduct.defaultColor])
  );
  $('.input-product-price').val(
    numberWithCommas(currentProduct.finalPrice[currentProduct.defaultColor])
  );
  $('.input-product-quantity').val(1);
  $('.input-product-color').val(currentProduct.defaultColor);
  $('.input-product-name').val(currentProduct.name);
};
const discountPercent = (quantity) => 1;
handleInitData();
// Models end

// Discount start
const handleBOGO = () => {
  const cartData = JSON.parse(localStorage.getItem('cart')) || [];
  const quantity = cartData.reduce(
    (prev, { quantity, id }) =>
      id.startsWith('onyx-tray') ? prev : prev + quantity,
    0
  );
  const freeTrayInfo = productInfos.find(
    ({ id: productId }) => productId === 'onyx-tray-free'
  );
  const freeTray = {
    ...freeTrayInfo,
    beforePrice: freeTrayInfo.beforePrice['black'],
    price: freeTrayInfo.finalPrice['black'],
    color: 'black',
    quantity: quantity,
    beforeTotalPrice: quantity * freeTrayInfo.beforePrice['black'],
    totalPrice: quantity * freeTrayInfo.finalPrice['black'],
    img: freeTrayInfo.img['black'],
  };
  if (quantity === 0) {
    const newCartData = cartData.filter(({ id }) => id !== 'onyx-tray-free');
    localStorage.setItem('cart', JSON.stringify(newCartData));
    return;
  }
  const existCartItem = cartData.find(({ id }) => id === 'onyx-tray-free');
  if (existCartItem) {
    existCartItem.quantity = quantity;
    localStorage.setItem('cart', JSON.stringify(cartData));
    return;
  }
  const newCartData = [...cartData, freeTray];
  localStorage.setItem('cart', JSON.stringify(newCartData));
};

const handleShowPopup = () => {
  if (sessionStorage.getItem('showPopup') === null) {
    sessionStorage.setItem('showPopup', true);
  }
  const showPopup = sessionStorage.getItem('showPopup');

  if (showPopup === 'true') {
    $('.sec-banner').removeClass('hidden');
    $('.banner-mask').on('click', () => {
      sessionStorage.setItem('showPopup', false);
      $('.sec-banner').addClass('hidden');
    });
    $('.banner-content a').on('click', () => {
      sessionStorage.setItem('showPopup', false);
      $('.sec-banner').addClass('hidden');
    });
  } else {
    $('.sec-banner').addClass('hidden');
  }
};
setTimeout(() => handleShowPopup(), 3000);

// Discount end

// Components start
const openSideBar = () => {
  lockBodyScroll();
  $('#email-form').show();
  $('.checkout-success').hide();
  $('.sidebar').addClass('active');
  $('.sidebar-mask').addClass('active');
  $('.sidebar-content-wrapper').addClass('active');
};
const hideSideBar = (cb) => {
  unlockBodyScroll();
  $('.sidebar').removeClass('active');
  $('.sidebar-mask').removeClass('active');
  $('.sidebar-content-wrapper').removeClass('active');
  cb();
};
const handleHeader = () => {
  $('.header-menu-toggle').on('click', (e) => {
    if ($('.header-menu-mobile').hasClass('show')) {
      unlockBodyScroll();
    } else {
      lockBodyScroll();
    }
    $('.header-menu-toggle').toggleClass('show');
    $('.header-menu-mobile').toggleClass('show');
  });
  $('.cart-menu').on('click', (e) => {
    openSideBar();
  });
};
const handleSidebar = () => {
  const heightSidebarHeader = $('.sidebar-header').height();
  const heightSidebarFooter = $('.sidebar-footer').height();
  $('.sidebar-body').css({
    height: `calc(100% - ${heightSidebarFooter}px - ${heightSidebarHeader}px)`,
  });
};
const createInputQuantity = (wrapper, { onChange }) => {
  const quantityAddBtn = $(wrapper).find('.js-quantity-add-btn');
  const quantityMinusBtn = $(wrapper).find('.js-quantity-minus-btn');
  const quantityInput = $(wrapper).find('.js-quantity-input');
  quantityInput.inputFilter(function (value) {
    if (+value > 99) return false;
    return /^[1-9][0-9]*$/.test(value);
  });

  const handleChangeQuantity = (value) => {
    const currentValue = +quantityInput.val();
    const result = currentValue + value;
    if (result === 0) return;
    quantityInput.val(result);
    if (onChange) onChange(result);
  };
  quantityInput.on('input', function () {
    onChange(+$(this).val());
  });
  quantityAddBtn.on('click', (e) => handleChangeQuantity(1));
  quantityMinusBtn.on('click', (e) => handleChangeQuantity(-1));
  return {
    changeQuantity: handleChangeQuantity,
  };
};

const createSwiperThumb = (thumb, main) => {
  const swiper = new Swiper(thumb, {
    spaceBetween: 12,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
  });
  const swiper2 = new Swiper(main, {
    spaceBetween: 0,
    thumbs: {
      swiper: swiper,
    },
  });
};

const initJSForm = (selector, { onSubmit }) => {
  const showInputError = (ele, message) => {
    const eleWrapper = ele.parent();
    const errMess = eleWrapper.find('.error-message');
    errMess.text(message);
    errMess.addClass('show');
    ele.addClass('input-error');
  };
  const hideInputError = (ele, message) => {
    const eleWrapper = ele.parent();
    const errMess = eleWrapper.find('.error-message');
    errMess.text(message);
    errMess.removeClass('show');
    ele.removeClass('input-error');
  };
  const checkRule = (ele, value) => {
    const rules = ele.data();
    if (rules.required && isEmpty(value)) {
      showInputError(ele, rules.requiredMessage);
      return false;
    } else {
      hideInputError(ele, rules.requiredMessage);
    }
    if (rules.isPhone && !isEmpty(value) && !isPhone(value)) {
      showInputError(ele, rules.isPhoneMessage);
      return false;
    } else {
      hideInputError(ele, rules.requiredMessage);
    }
    if (rules.isEmail && !isEmpty(value) && !isEmail(value)) {
      showInputError(ele, rules.isEmailMessage);
      return false;
    } else {
      hideInputError(ele, rules.requiredMessage);
    }
    return true;
  };
  const validate = (parsedFormData) => {
    let hasError = false;
    parsedFormData.forEach(({ name, value }) => {
      const input = $(`.input[data-name=${name}]`);
      if (input.length > 0) {
        const ruleResult = checkRule(input, value);
        if (!ruleResult) hasError = true;
      }
    });
    return hasError;
  };
  $(selector).submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    const parsedFormData = [...formData.entries()].map((dataObject) => ({
      name: dataObject[0],
      value: dataObject[1],
    }));
    const hasError = validate(parsedFormData);
    if (hasError) {
      $('.error-message.show')[0].scrollIntoView({
        behavior: 'smooth',
        block: 'center',
      });
      return;
    }
    if (onSubmit) onSubmit(e, parsedFormData);
  });
  const resetValidate = () => {
    $('.input').each(function () {
      hideInputError($(this));
    });
  };
  const resetStatus = () => {
    $(selector).css('display', 'grid');
    $('.w-form-done').css('display', 'none');
  };
  return {
    resetValidate,
    resetStatus,
    submit: $(selector).submit,
  };
};
const cartItemComponent = (cartItem) => {
  return `<div
    id="w-node-d4e1ca8a-f320-735f-b27c-40999885d7eb-de0294ff"
    class="cart-item"
    data-id="${cartItem.id}"
    >
    <div class="cart-item-id"></div>
    <div class="cart-item-img-wrapper">
      <img
        src="${cartItem.img}"
        alt=""
        class="cart-item-img"
      />
    </div>
    <div class="cart-item-info-wrapper">
      <div class="cart-item-info">
        <div class="cart-item-content-wrapper">
          <div class="text cart-item-name">${cartItem.name}</div>
          <div class="cart-item-color" data-cart-color="${
            cartItem.color
          }">Màu: ${cartItem.color}</div>
        </div>
        <div class="cart-item-price-wrapper">
          ${
            cartItem.id === 'onyx-tray-free'
              ? `<div class="cart-item-price remove-text">${numberWithCommas(
                  cartItem.removePrice * cartItem.quantity
                )}</div>`
              : ''
          }
            <div class="cart-item-price">${numberWithCommas(
              cartItem.beforeTotalPrice
            )}</div>
          <div class="cart-item-vnd">vnđ</div>
        </div>
      </div>
      <div class="cart-item-control">
      ${
        cartItem.id === 'onyx-tray-free'
          ? `<div class="text cart-item-quantity-text">Số lượng : ${cartItem.quantity}</div>`
          : `<div class="cart-quantity">
      <div class="cart-quantity-minus js-quantity-minus-btn w-embed">
        <svg
          width="12"
          height="12"
          viewBox="0 0 12 12"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M11 7H1C0.734784 7 0.48043 6.89464 0.292893 6.70711C0.105357 6.51957 0 6.26522 0 6C0 5.73478 0.105357 5.48043 0.292893 5.29289C0.48043 5.10536 0.734784 5 1 5H11C11.2652 5 11.5196 5.10536 11.7071 5.29289C11.8946 5.48043 12 5.73478 12 6C12 6.26522 11.8946 6.51957 11.7071 6.70711C11.5196 6.89464 11.2652 7 11 7Z"
            fill="#212121"
          ></path>
        </svg>
      </div>
      <div class="cart-quantity-input-wrapper w-embed">
        <input class="cart-quantity-input js-quantity-input" value="${cartItem.quantity}" />
      </div>
      <div class="cart-quantity-add js-quantity-add-btn w-embed">
        <svg
          width="12"
          height="12"
          viewBox="0 0 12 12"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M11 5H7V1C7 0.734784 6.89464 0.48043 6.70711 0.292893C6.51957 0.105357 6.26522 0 6 0C5.73478 0 5.48043 0.105357 5.29289 0.292893C5.10536 0.48043 5 0.734784 5 1V5H1C0.734784 5 0.48043 5.10536 0.292893 5.29289C0.105357 5.48043 0 5.73478 0 6C0 6.26522 0.105357 6.51957 0.292893 6.70711C0.48043 6.89464 0.734784 7 1 7H5V11C5 11.2652 5.10536 11.5196 5.29289 11.7071C5.48043 11.8946 5.73478 12 6 12C6.26522 12 6.51957 11.8946 6.70711 11.7071C6.89464 11.5196 7 11.2652 7 11V7H11C11.2652 7 11.5196 6.89464 11.7071 6.70711C11.8946 6.51957 12 6.26522 12 6C12 5.73478 11.8946 5.48043 11.7071 5.29289C11.5196 5.10536 11.2652 5 11 5Z"
            fill="#212121"
          ></path>
        </svg>
      </div>
    </div>
    <div class="cart-remove">
      <div class="text text-cart-remove">Xóa</div>
    </div>`
      }
        
      </div>
    </div>
    </div>
        `;
};
const cartPreviewItemComp = (cartItem) =>
  `<div class="cart-item">
  <div data-id="${cartItem.id}" class="cart-item-id"></div>
  <div class="cart-item-img-wrapper">
    <img
      src="${cartItem.img}"
      alt=""
      class="cart-item-img"
    />
  </div>
  <div class="cart-item-info-wrapper">
    <div class="cart-item-info">
      <div class="cart-item-content-wrapper">
        <div class="text cart-item-name">${cartItem.name}</div>
        <div class="cart-item-color">Màu: ${cartItem.color}</div>
      </div>
      <div class="cart-item-price-wrapper">
        <div class="cart-item-price">${numberWithCommas(
          cartItem.beforeTotalPrice
        )}</div>
        <div class="cart-item-vnd">vnđ</div>
      </div>
    </div>
    <div class="text cart-quantity-text">Số lượng: ${cartItem.quantity}</div>
  </div>
</div>
`;
const initCart = () => {
  let currrentStep = 'add-cart';
  const checkoutForm = initJSForm('#email-form', {
    onSubmit: (e, value) => {
      $('.btn-checkout').text('Please wait...');
      $('.btn-checkout').val('Please wait...');
      $('.btn-checkout').addClass('pointer-event-none');
      const cartData = JSON.parse(localStorage.getItem('cart')) || [];
      // const totalCartQuantity = cartData.reduce(
      //   (prev, { quantity }) => prev + quantity,
      //   0
      // );
      const cartDataParse = cartData.map((item) => {
        return `${item.name} (${item.color}) x${
          item.quantity
        } --- ${numberWithCommas(item.beforeTotalPrice)} vnđ`;
      });
      const productInfo = cartDataParse.join('\n');
      const totalCartPriceBefore = cartData.reduce(
        (prev, { beforeTotalPrice }) => prev + beforeTotalPrice,
        0
      );
      const totalCartPrice = cartData.reduce(
        (prev, { totalPrice }) => prev + totalPrice,
        0
      );
      let valueObj = {};
      value.forEach(({ name, value }) => (valueObj[name] = value));
      const data = {
        fields: [
          {
            name: 'email',
            value: valueObj['email'],
          },
          {
            name: 'note',
            value: valueObj['note'],
          },
          {
            name: 'customer_info',
            value: `${valueObj['lastname']} - ${valueObj['phone']}`,
          },
          {
            name: 'address',
            value: `${valueObj.customer_address} (${valueObj.address_type} address)`,
          },
          {
            name: 'product_info',
            value: productInfo,
          },
          {
            name: 'sub_total',
            value: `${numberWithCommas(totalCartPriceBefore)} vnd`,
          },
          {
            name: 'discount',
            value: `${numberWithCommas(
              totalCartPriceBefore - totalCartPrice
            )} vnd`,
          },
          {
            name: 'total_price',
            value: `${numberWithCommas(totalCartPrice)} vnd`,
          },
        ],
        context: {
          pageUri: window.location.href,
          pageName: 'Cart checkout',
        },
      };
      const final_data = JSON.stringify(data);
      $.ajax({
        url: sendSubmissionUrl,
        method: 'POST',
        data: final_data,
        dataType: 'json',
        headers: {
          accept: 'application/json',
          'Access-Control-Allow-Origin': '*',
        },
        contentType: 'application/json',
        success: function () {
          setTimeout(() => {
            localStorage.setItem('cart', '[]');
            currrentStep = 'checkout-success';
            $('.btn-checkout').text('Tiếp tục mua hàng');
            $('.btn-checkout').val('Tiếp tục mua hàng');
            $('.btn-checkout').removeClass('pointer-event-none');
            $(e.target).hide();
            const parent = $(e.target).parent();
            $('.checkout-success').show();
            value.forEach(({ name, value }) => {
              $(`[data-name=${name}]`).text(value === '' ? 'N/A' : value);
            });
            $('.cart-count').text(0);
            $('.cart-count-mobile').hide();
            $('.sidebar-back-ic').addClass('hide');
            $('.text-cart').text('Xin cảm ơn');
          }, 800);
        },
        error: function () {
          $('.btn-checkout').text('Đặt hàng ngay');
          $('.btn-checkout').val('Đặt hàng ngay');
          $('.btn-checkout').removeClass('pointer-event-none');
        },
      });
    },
  });
  if (isEmptyArray(JSON.parse(localStorage.getItem('cart')))) {
    localStorage.setItem('cart', '[]');
  }
  const cartWrapper = $('.cart-wrapper');
  const cartPreivewWrapper = $('.cart-preview-info-wrapper');
  const updateQuantityCartItem = (cartItem, quantity) => {
    const newQuantity = cartItem.quantity + quantity;
    cartItem.quantity = newQuantity;
    cartItem.beforeTotalPrice = newQuantity * cartItem.beforePrice;
    cartItem.totalPrice = newQuantity * cartItem.price;
  };
  const addItemToCart = (cartItem) => {
    const cartData = JSON.parse(localStorage.getItem('cart')) || [];
    const existCartItem = cartData.find(
      ({ id, color }) => id === cartItem.id && color === cartItem.color
    );
    if (existCartItem) {
      updateQuantityCartItem(existCartItem, cartItem.quantity);
      localStorage.setItem('cart', JSON.stringify(cartData));
      renderCart();
      return;
    }
    const newCartData = [...cartData, cartItem];
    localStorage.setItem('cart', JSON.stringify(newCartData));
    renderCart();
  };
  const removeItemFromCart = (cartItem) => {
    const cartData = JSON.parse(localStorage.getItem('cart')) || [];
    const newCartData = cartData.filter(
      ({ id, color }) => id !== cartItem.id || color !== cartItem.color
    );
    localStorage.setItem('cart', JSON.stringify(newCartData));
    renderCart();
  };
  const renderCart = () => {
    handleBOGO();
    (() => {
      const a = JSON.parse(localStorage.getItem('cart')) || [];
      a.sort((a, b) => a.sort - b.sort);
      localStorage.setItem('cart', JSON.stringify(a));
    })();
    const cartData = JSON.parse(localStorage.getItem('cart')) || [];
    const totalCartQuantity = cartData.reduce(
      (prev, { quantity }) => prev + quantity,
      0
    );
    $('.cart-count').text(totalCartQuantity);
    if (totalCartQuantity === 0) {
      $('.sidebar-footer').addClass('empty');
      $('.cart-count-mobile').hide();
      cartWrapper.html(
        `<div class="cart-empty active"><div class="text-cart-empty"><strong>Giỏ hàng của bạn trống</strong></div></div>`
      );
      return;
    } else {
      $('.sidebar-footer').removeClass('empty');
      $('.cart-count-mobile').show();
      const cartDataEle = cartData.map(cartItemComponent);
      const cartPreviewDataEle = cartData.map(cartPreviewItemComp);
      cartWrapper.html(cartDataEle.join(' '));
      cartPreivewWrapper.html(cartPreviewDataEle.join(' '));
    }

    const totalCartPriceBefore = cartData.reduce(
      (prev, { beforeTotalPrice }) => prev + beforeTotalPrice,
      0
    );
    const totalCartPrice = cartData.reduce(
      (prev, { totalPrice }) => prev + totalPrice,
      0
    );
    $('.text-subtotal-price')
      .eq(0)
      .text(numberWithCommas(totalCartPriceBefore));
    $('.text-subtotal-price')
      .eq(1)
      .text(numberWithCommas(totalCartPrice - totalCartPriceBefore));
    $('.text-subtotal-price').eq(2).text(numberWithCommas(totalCartPrice));
    $('.cart-quantity').each(function () {
      const cartItemEle = $(this).closest('.cart-item');
      const cartItemId = cartItemEle.data('id');
      const cartItemColor = cartItemEle
        .find('.cart-item-color')
        .data('cart-color');
      const cartItem = cartData.find(
        ({ id, color }) => id === cartItemId && color === cartItemColor
      );
      createInputQuantity($(this), {
        onChange: (quantity) => {
          cartItem.quantity = quantity;
          cartItem.beforeTotalPrice = quantity * cartItem.beforePrice;
          cartItem.totalPrice = quantity * cartItem.price;
          localStorage.setItem('cart', JSON.stringify(cartData));
          renderCart();
        },
      });
    });
    $('.cart-remove').on('click', function () {
      const cartItemEle = $(this).closest('.cart-item');
      const cartItemId = cartItemEle.data('id');
      const cartItemColor = cartItemEle
        .find('.cart-item-color')
        .data('cart-color');
      const cartItem = cartData.find(
        ({ id, color }) => id === cartItemId && color === cartItemColor
      );
      removeItemFromCart(cartItem);
      renderCart();
    });
  };

  const openFormCheckoutCart = () => {
    currrentStep = 'checkout';
    checkoutForm.resetValidate();
    $('.cart-checkout-form-wrapper').addClass('active');
    $('.cart-wrapper').addClass('hide');
    $('.text-cart').text('Thông tin cá nhân');
    $('.btn-checkout').text('Thanh toán ngay');
    $('.sidebar-back-ic').removeClass('hide');
    $('.cart-subtotal').hide();
    $('.sidebar-back-ic').on('click', () => {
      hideFormCheckoutCart();
    });
    handleSidebar();
  };
  const hideFormCheckoutCart = () => {
    currrentStep = 'add-cart';
    $('.cart-checkout-form-wrapper').removeClass('active');
    $('.cart-wrapper').removeClass('hide');
    $('.text-cart').text('Giỏ hàng');
    $('.btn-checkout').text('Thanh toán ngay');
    $('.sidebar-back-ic').addClass('hide');
    $('.cart-subtotal').show();
    handleSidebar();
  };
  $('.sidebar-close-ic').on('click', (e) => {
    hideSideBar(renderCart);
    hideFormCheckoutCart();
  });
  $('.sidebar-mask').on('click', (e) => {
    hideSideBar(renderCart);
    hideFormCheckoutCart();
  });
  const handleCheckoutCart = () => {
    $('.btn-checkout').on('click', function (e) {
      e.preventDefault();
      if (currrentStep === 'add-cart') {
        openFormCheckoutCart();
      } else {
        if (currrentStep === 'checkout') {
          $('#email-form').submit();
        } else {
          hideSideBar(renderCart);
          hideFormCheckoutCart();
        }
      }
    });
  };
  handleCheckoutCart();
  renderCart();
  return {
    addItemToCart,
    removeItemFromCart,
  };
};
var CART = initCart();
//Component ends

// Main app script start
/* HANDLE DETAIL PAGE */
const pageDetailScript = () => {
  $('.sc-modal .product-price-before-wrapper').addClass('hidden');
  const quantityHandler = createInputQuantity('.product-quantity', {
    onChange: (quantity) => {
      const totalPrice =
        currentProduct.finalPrice[
          $('.product-color-item.selected').data('color')
        ] * quantity;
      const priceBefore = $('.product-price-before-wrapper > div:nth-child(1)');
      const priceAfter = $('.product-price-after-wrapper > div:nth-child(1)');
      // if (quantity > 1) {
      //   $(".sc-modal .product-price-before-wrapper").removeClass("hidden");
      // } else {
      //   $(".sc-modal .product-price-before-wrapper").addClass("hidden");
      // }
      priceBefore.text(numberWithCommas(totalPrice));
      priceAfter.text(numberWithCommas(totalPrice * discountPercent(quantity)));
      $('.modal-product-quantity').text(quantity);

      $('.input-product-price').val(
        numberWithCommas(totalPrice * discountPercent(quantity))
      );
      $('.input-product-quantity').val(quantity);
    },
  });
  createSwiperThumb('.thumb-swiper-walnut', '.product-swiper-walnut');
  createSwiperThumb('.thumb-swiper-oak', '.product-swiper-oak');
  $('.product-color-item').each(function () {
    $(this).on('click', function () {
      $('.product-color-item').each(function () {
        $(this).removeClass('selected');
      });
      $(this).addClass('selected');
      const color = $(this).data('color');
      const quantity = +$('.input-product-quantity').val();
      const totalPrice =
        currentProduct.finalPrice[
          $('.product-color-item.selected').data('color')
        ] * quantity;
      const priceBefore = $('.product-price-before-wrapper > div:nth-child(1)');
      const priceAfter = $('.product-price-after-wrapper > div:nth-child(1)');
      priceBefore.text(numberWithCommas(totalPrice));
      priceAfter.text(numberWithCommas(totalPrice * discountPercent(quantity)));
      $('.product-image-wrapper').removeClass('show');
      $('.img-wrapper').removeClass('show');
      $('.order-confirm-img-wrapper').removeClass('show');
      $(`.product-image-${color}`).addClass('show');
      $(`.img-wrapper-${color}`).addClass('show');
      $(`.order-confirm-img-wrapper-${color}`).addClass('show');
      $('.modal-product-color').text(color);
      $('.text-color-spec').text(color);
      $('.text-price-heading').text(
        numberWithCommas(currentProduct.finalPrice[color])
      );
      $('.vnd-wrapper .heading.h4').text(
        numberWithCommas(currentProduct.finalPrice[color])
      );
      $('.vnd-before-wrapper .heading.h4').text(
        numberWithCommas(currentProduct.beforePrice[color])
      );
      $('.text-price-before-heading').text(
        numberWithCommas(currentProduct.beforePrice[color])
      );
      $('.input-product-color').val(color);
    });
  });
  const buyForm = initJSForm('#BuyForm', {
    onSubmit: (e, value) => {
      $('.btn-submit').text('Please wait...');
      $('.btn-submit').val('Please wait...');
      $('.btn-submit').addClass('pointer-event-none');
      let valueObj = {};
      value.forEach(({ name, value }) => (valueObj[name] = value));

      const priceProductInfo =
        currentProduct.beforePrice[valueObj.product_color] *
        valueObj.product_number;

      const priceProductFinal =
        currentProduct.finalPrice[valueObj.product_color] *
        valueObj.product_number;

      const priceDiscount = priceProductInfo - priceProductFinal;
      value.totalPrice = priceProductFinal;
      const data = {
        fields: [
          {
            name: 'email',
            value: valueObj['email'],
          },
          {
            name: 'note',
            value: valueObj['note'],
          },
          {
            name: 'customer_info',
            value: `${valueObj['lastname']} - ${valueObj['phone']}`,
          },
          {
            name: 'address',
            value: `${valueObj.customer_address} (${valueObj.address_type} address)`,
          },
          {
            name: 'sub_total',
            value: `${numberWithCommas(priceProductInfo)} vnd`,
          },
          {
            name: 'product_info',
            value: `${valueObj.product_name} (${valueObj.product_color}) x${
              valueObj.product_number
            } --- ${numberWithCommas(
              priceProductInfo
            )} vnd \n Onyx Tray Free (black) x${
              valueObj.product_number
            } --- 0 vnd`,
          },
          {
            name: 'discount',
            value: `${numberWithCommas(priceDiscount)} vnd`,
          },
          {
            name: 'total_price',
            value: `${numberWithCommas(priceProductFinal)} vnd`,
          },
        ],
        context: {
          pageUri: window.location.href,
          pageName: document.title,
        },
      };
      const final_data = JSON.stringify(data);

      $.ajax({
        url: sendSubmissionUrl,
        method: 'POST',
        data: final_data,
        dataType: 'json',
        headers: {
          accept: 'application/json',
          'Access-Control-Allow-Origin': '*',
        },
        contentType: 'application/json',
        success: function (response) {
          setTimeout(() => {
            $('.btn-submit').text('Đặt hàng ngay');
            $('.btn-submit').val('Đặt hàng ngay');
            $('.btn-submit').removeClass('pointer-event-none');
            $(e.target).hide();
            const parent = $(e.target).parent();
            $('.w-form-done').show();
            value.forEach(({ name, value }) => {
              $(`[data-name=${name}]`).text(value === '' ? 'N/A' : value);
            });
            $(`[data-name=total_price]`).text(
              numberWithCommas(priceProductFinal)
            );
          }, 800);
        },
        error: function () {
          $('.btn-submit').text('Đặt hàng ngay');
          $('.btn-submit').val('Đặt hàng ngay');
          $('.btn-submit').removeClass('pointer-event-none');
        },
      });
    },
  });
  $('.btn-buy-now').on('click', function () {
    lockBodyScroll();
    $('.sc-modal').addClass('show');
    buyForm.resetValidate();
  });
  $('.btn-add-to-cart').on('click', function () {
    const quantity = +$('.product-quantity-input').val();
    const color = $('.product-color-item.selected').data('color');
    const cartItem = {
      ...currentProduct,
      beforePrice: currentProduct.beforePrice[color],
      price: currentProduct.finalPrice[color],
      color: color,
      quantity: quantity,
      beforeTotalPrice: quantity * currentProduct.beforePrice[color],
      totalPrice: quantity * currentProduct.finalPrice[color],
      img: currentProduct.img[color],
    };
    CART.addItemToCart(cartItem);
    quantityHandler.changeQuantity(-$('.product-quantity input').val() + 1);
    openSideBar();
  });
  $('.modal-close').on('click', function () {
    unlockBodyScroll();
    $('.sc-modal').removeClass('show');
    $('body').removeClass('modal-open');
    setTimeout(() => {
      buyForm.resetStatus();
    }, 500);
  });
};
/* HANDLE HOME */
const handleHome = () => {
  $('.link-block-dual').hover(() => {
    $('.single').removeClass('show');
    $('.dual').addClass('show');
  });
  $('.link-block-single').hover(() => {
    $('.single').addClass('show');
    $('.dual').removeClass('show');
  });
};
// Main app script end
handleHeader();
handleSidebar();
if (!onHomePage) pageDetailScript();
else handleHome();
